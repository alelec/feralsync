=========
Feralsync
=========
This is a download client for working with feralserver

It'll automatically move files from the remote feralserver to the local pc, removing them from the remote once done.

All interaction happens with the sync client over the built in web interface, although this can be used as a standalone app with gui.py if desired.

Installation
------------

You will need Python 3.5 or newer installed with pip and virtualenv packages pre-installed.::

**Linux / Mac**

For debian/ubuntu based distros::

	sudo apt install python3 python3-pip

Install into system::

    cd /usr/local/share
    sudo python3 -m venv feralsync
    cd feralsync/
    sudo ./bin/pip install --extra-index-url https://alelec.gitlab.io/feralsync feralsync
    sudo ln -s `pwd`/bin/feral_sync /usr/local/bin/

    feral_sync

Then browse to http://localhost:45211/

To permanently run in background, check the service example script in services folder.

**Windows**

You'll need git installed for this install process. If you install choco from https://chocolatey.org/install all the dependencies can be installed with::

    choco install -y python3 vcredist2015 git

Otherwise, install python >= 3.5 from https://www.python.org/downloads/windows/

You may also need to install vcredist 2015 from https://www.microsoft.com/en-au/download/details.aspx?id=48145

Make sure you install x86 / x64 version same as the python installed.

and git from https://git-scm.com/download/win

make sure at least git gets installed into the path when asked.

Now to install feralsync, from command prompt, installing into home directory::

    pushd %HOMEPATH%

Or administrator cmd prompt, install into program files::

    pushd %PROGRAMFILES%

Install feral sync::

    py -3 -m venv feralsync
    cd feralsync
    Scripts\pip install --extra-index-url https://alelec.gitlab.io/feralsync feralsync[GUI]

Add shortcut to start menu::

    powershell

    $startmenu = "$env:APPDATA\Microsoft\Windows\Start Menu\Programs\FeralSync"
    mkdir $startmenu
    $feralsync = (Get-Item -Path ".\" -Verbose).FullName

    $AppLocation = "$feralsync\Scripts\FeralSync.exe"
    $WshShell = New-Object -ComObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut("$startmenu\FeralSync.lnk")
    $Shortcut.TargetPath = $AppLocation
    $Shortcut.IconLocation = "$feralsync\icon\feralsync.ico"
    $Shortcut.Description ="FeralSync client"
    $Shortcut.WorkingDirectory ="$feralsync"
    $Shortcut.Save()
