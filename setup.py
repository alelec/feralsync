import os
import sys
from setuptools import setup


with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    long_description = readme.read()


setup(
    name='feralsync',
    packages=['feralsync'],
    include_package_data=True,
    description='Client to download from remote feralserver',
    long_description=long_description,
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    url='https://gitlab.alelec.net/corona/feralsync',
    # use_scm_version={'local_scheme': lambda _:""},
    use_scm_version=True,
    setup_requires=['setuptools_scm', 'setuptools_git'],
    install_requires=['aiohttp', 'aiohttp-jinja2', 'PyYAML', 'requests',
                      'structured_config', 'transmissionrpc', 'aiofiles',
                      'setuptools_scm'],
    extras_require={
        'GUI':  ["wxpython>=4.0.0a1", "cefpython3"],
    },
    entry_points={
        'console_scripts': [
            'feral_sync = feralsync.feral_sync:main',
        ],
        'gui_scripts': [
            'FeralSync = feralsync.gui:main [GUI]',
        ]
    },
    options={
        'app': {
            'formal_name': 'FeralSync'
        },

        # Desktop/laptop deployments
        'macos': {
            'app_requires': ["wxpython>=4.0.0a1", "cefpython3"],
            'icon': 'feralsync/icon/feralsync'
        },
        'linux': {
            'app_requires': ["wxpython>=4.0.0a1", "cefpython3"],
        },
        'windows': {
            'app_requires': ["wxpython>=4.0.0a1", "cefpython3"],
            'icon': 'feralsync/icon/feralsync'
        }
    }
)