# -*- mode: python -*-

block_cipher = None

added_files = [
  ('templates', 'templates'),
  ('static', 'static')
]

a = Analysis(['feral_sync.py'],
             pathex=['/Volumes/SpindMac/Users/corona/src/feralsync/feralsync_src'],
             binaries=None,
             datas=added_files,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='feral_sync',
          debug=False,
          strip=False,
          upx=True,
          console=True )
