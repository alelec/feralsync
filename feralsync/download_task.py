import time
import logging
import asyncio
from collections import OrderedDict
from . import client


class ProgressHandler(object):
    current = OrderedDict()
    finished = OrderedDict()

    _rate_ave_period = 2  # approx period in seconds over which the download rate it averaged
    _download_rates = {}
    _rate_tracking = {}
    _closed = False

    status = "Startup"
    scanning = False

    @classmethod
    def close(cls):
        cls._closed = True

    @classmethod
    async def rate_calc(cls):
        self = cls

        prev = time.time() - self._rate_ave_period
        while not self._closed:
            try:
                now = time.time()
                for name, downloaded in self._rate_tracking.items():
                    period = now - prev
                    self._download_rates[name] = downloaded / period
                    self._rate_tracking[name] = 0
                prev = now
            except:
                logging.getLogger('Download').exception("rate_calc error")
            finally:
                await asyncio.sleep(self._rate_ave_period)

    @classmethod
    def download_rate(cls, name = None):
        """
        :param (str or None) name: filename or None for global rate
        :return: download rate in bytes per second
        """
        return cls._download_rates.get(name, 0)

    @classmethod
    def rate_notify(cls, local_filename, add_size):
        for key in (local_filename, None):
            cls._rate_tracking[key] = cls._rate_tracking.get(key, 0) + add_size

    @classmethod
    def register(cls, remote_file, remote_size, local_filename):
        task = Progress(remote_file, remote_size, local_filename)
        cls.current[local_filename] = task
        return task

    @classmethod
    async def task_cleanup(cls):
        while not cls._closed:
            try:
                old = []
                now = time.time()
                display_time = 60 * 60 * 6
                for key, task in cls.finished.items():
                    task = task  # type: Progress
                    if task.status == Progress.STATUS_PURGE or \
                            (task.finished.is_set() and ((now - task.updated) > display_time)):
                        old.append(key)
                for key in old:
                    del cls.finished[key]
            except:
                logging.getLogger('Download').exception("task_cleanup")
            await asyncio.sleep(30)

    @classmethod
    def notify_done(cls, local_filename):
        task = cls.current.pop(local_filename)
        if task:
            cls.finished[local_filename] = task


class Progress(client.Progress):

    STATUS_NEW    = "STATUS_NEW"
    STATUS_START  = "STATUS_START"
    STATUS_ACTIVE = "STATUS_ACTIVE"
    STATUS_PAUSED = "STATUS_PAUSED"
    STATUS_VERIFY = "STATUS_VERIFY"
    STATUS_DONE   = "STATUS_DONE"
    STATUS_DELETE = "STATUS_DELETE"
    STATUS_ERROR  = "STATUS_ERROR"
    STATUS_PURGE  = "STATUS_PURGE"

    _WINDOW = 10

    def __init__(self, remote_file, remote_size, local_filename):
        """
        :param str remote_file: remote file path/name
        :param int remote_size: remote file size
        :param local_filename: local file path to save to (via temp file)
        """
        self.remote_file = remote_file
        self.local_filename = local_filename
        self.length = remote_size

        self.future = None
        self.current = 0
        self.updated = time.time()

        self.finished = asyncio.Event()
        self._status = self.STATUS_NEW
        self.error_msg = None

        self.done = asyncio.Event()

        self.log = logging.getLogger('Download')
        super(Progress, self).__init__()

    async def notify_verifying(self):
        """
        Called when the download transfer is complete and about to be verified
        :return: None
        """
        self.status = self.STATUS_VERIFY

    async def notify_done(self, error):
        """
        Called when the download task is finished
        :param str or None error: Whether the download succeeded
        :return: None
        """

        self.enabled.clear()
        self.done.set()

        self.error_msg = error

        if not error:

            self.log.info("Finished: %s" % self.local_filename)
            if self.status == self.STATUS_NEW:
                # Previously downloaded
                self.status = self.STATUS_PURGE
            else:
                self.status = self.STATUS_DONE
        else:
            self.log.error("%s: %s" % (self.local_filename,error))
            self.status = self.STATUS_ERROR

        ProgressHandler.notify_done(self.local_filename)


    async def progress_update(self, add_size):
        """
        :param int add_size: chunk size just downloaded
        :return: None
        """
        if self.enabled.is_set():
            self.status = self.STATUS_ACTIVE

        self.current += add_size
        ProgressHandler.rate_notify(self.local_filename, add_size)

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, s):
        self.updated = time.time()
        self._status = s
        if s in (self.STATUS_DONE, self.STATUS_ERROR):
            self.finished.set()

    @property
    def rate(self):
        return ProgressHandler.download_rate(self.local_filename)

    async def Pause(self):
        self.status = self.STATUS_PAUSED
        self.enabled.clear()
        self.done.set()

    async def Resume(self):
        self.status = self.STATUS_ACTIVE
        self.enabled.set()

    async def Cleanup(self):
        self.status = self.STATUS_PURGE

    @property
    async def Paused(self):
        return not self.enabled.is_set()
