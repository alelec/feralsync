###############################################################
# -*- coding: utf-8 -*-
#!/usr/bin/env python3

__author__ = "Andrew Leech"
__copyright__ = "Copyright 2015, alelec"
__license__ = "GPL"
__version__ = "1.0.0a"
__maintainer__ = "Andrew Leech"
__email__ = "andrew@alelec.net"
__status__ = "Development"

import os
from structured_config import Structure, ConfigFile, IntField

class Target(Structure):
    """
    Structure for holding the remote <-> local sync path mapping
    """
    remote_path = '/'
    local_path = '~/'


# Default configuration for the application

class config(Structure):

    class server(Structure):
        url = 'https://selene.feralhosting.com/corona/feralserver/downloads'
        username = '<user>'
        password = '<password>'

    sync_targets = [
        Target(
            remote_path="/other/",
            local_path="~/downloads/"
        ),
        Target(
            remote_path="/tvshows/",
            local_path="~/tvshows/"
        ),
        Target(
            remote_path="/movies/",
            local_path="~/movies/"
        )
    ]

    concurrent_transfers = IntField(3)

    # Max number of tcp connections at any one time
    concurrent_connections =  IntField(32)

    # Local web ui port
    ui_server_port = IntField(45211)

    # sentry dsn for diagnostics
    raven_dsn = None

    # open browser window on startup
    browser_on_launch = True

# Handle upgrade to new config file location
if os.path.exists("config.yaml"):
    print("Moving config file to new location, please restart")
    config_file = ConfigFile('config.yaml', config, appname="feralsync")
    os.rename("config.yaml", config_file.config_path)
    exit()

config_file = ConfigFile('config.yaml', config, appname="feralsync")
Config = config_file.config  # type: config
