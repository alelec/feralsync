###############################################################
# -*- coding: utf-8 -*-
# !/usr/bin/env python3

__author__ = "Andrew Leech"
__copyright__ = "Copyright 2015, alelec"
__license__ = "GPL"
__version__ = "1.0.0a"
__maintainer__ = "Andrew Leech"
__email__ = "andrew@alelec.net"
__status__ = "Development"

import os
import sys
import time
import json
import logging
import jinja2
import asyncio
import aiohttp
import threading
import aiohttp_jinja2
from aiohttp import web
from aiohttp_jinja2 import template

from collections import OrderedDict
from functools import wraps
from urllib.parse import urlparse, urlunsplit

from . import config
from . import updater
from . import transmission
from .future_thread import Future
from .download_task import Progress, ProgressHandler

try:
    import raven
    sentry = raven.Client(config.Config.raven_dsn)
except ImportError:
    sentry = None


Updater = updater.Updater(package_name="feralsync",
                          extra_index="https://alelec.gitlab.io/feralsync")


def log_exception(exception=Exception):
    def deco(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception as err:
                if sentry is not None:
                    sentry.captureException()
                raise

        return wrapper

    return deco


if getattr(sys, 'frozen', False):
    # running in a bundle
    frozen = True
else:
    # running live
    frozen = False


class UIDetail(object):
    STATUS_MAP = {
        Progress.STATUS_NEW:    "New",
        Progress.STATUS_START:  "Start",
        Progress.STATUS_ACTIVE: "Active",
        Progress.STATUS_PAUSED: "Paused",
        Progress.STATUS_DONE:   "Done",
        Progress.STATUS_VERIFY: "Verifying",
        Progress.STATUS_DELETE: "Cleanup",
        Progress.STATUS_ERROR:  "Error",
        Progress.STATUS_PURGE:  "Done",
    }

    def __init__(self):
        self.filename = None
        self.last_update = None
        self.filesize = None  # type: int
        self.percent = None
        self.rate = None
        self.tooltip = None
        self._status = None

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, s):
        self._status = self.STATUS_MAP.get(s, 'Unknown')

    def todict(self):
        return dict(filename=self.filename, last_update=self.last_update, percent=self.percent,
                    filesize=self.filesize, rate=self.rate, status=self.status, tooltip=self.tooltip)


class ProgressUI(object):
    def __init__(self, feral):
        """

        :param feral_sync.FeralSync feral:
        """

        self._log = logging.getLogger("webapp")
        self._log.setLevel(logging.WARN)

        if frozen:
            static_dir = os.path.join(sys._MEIPASS, 'static')
            template_dir = os.path.join(sys._MEIPASS, 'templates')
        else:
            static_dir = os.path.join(os.path.dirname(__file__), 'static')
            template_dir = os.path.join(os.path.dirname(__file__), 'templates')


        logging.getLogger('aiohttp').setLevel(logging.WARN)

        self._app = web.Application()
        self._app.router.add_route('GET', '/', self.index)
        self._app.router.add_route('GET', '/progress.json', self.progress)
        self._app.router.add_route('GET', '/ws', self.websocket_handler)
        self._app.router.add_route('GET', '/settings', self.settings_websocket)
        self._app.router.add_route('POST', '/settings', self.settings_post, expect_handler=aiohttp.web.Request.json)
        self._app.router.add_route('POST', '/add_torrent', self.add_torrent, expect_handler=aiohttp.web.Request.json)
        self._app.router.add_static("/", static_dir)

        self._app.make_handler(access_log=None)

        aiohttp_jinja2.setup(self._app, loader=jinja2.FileSystemLoader(template_dir))

        self.feral = feral
        self._srv = None
        self._handler = None

        self._app['websockets'] = []

        self._cached_progress = {}
        self._update_progress = threading.Event()
        self._cache_progress_future = None  # type: Future

    # # @log_exception()
    @template('home.html')
    async def index(self, request):
        user = config.Config.server.username
        fileserver_server = urlparse(config.Config.server.url)
        remote = lambda p: urlunsplit(fileserver_server[0:2] + (p.format(user=user), '', ''))
        return {"sickrage_url": remote("/corona/sickrage_{user}/home"),
                "couch_url": remote("/corona/couchpotato_{user}/home/"),
                "transmission_url": remote("/corona/transmission_{user}/web/")}

    async def add_torrent(self, request):
        data = await request.json()
        try:
            ProgressHandler.status = "Sending torrent... "
            loop = asyncio.get_event_loop()
            torrent = await loop.run_in_executor(None, transmission.add_torrent,
                                                 data['url'], data['file'], data['type'])

            ProgressHandler.status = "Checking torrent... "
            await asyncio.sleep(0.25)
            await loop.run_in_executor(None, torrent.update)
            ProgressHandler.status = "Added torrent: %s" % torrent.name
        except Exception as ex:
            ProgressHandler.status ="Torrent error: %s" % ex
            logging.getLogger("transmission").exception("During add")
        return web.Response()

    async def progress(self, request):

        details = self._progress()
        resp = web.json_response(details)
        return resp

    def _progress(self):
        return self._cached_progress

    def thread_cache_progress(self):
        while not self._cache_progress_future or not self._cache_progress_future.cancelled():
            try:
                if len(self._app['websockets']) > 0:
                    self._cached_progress = self._calculate_progress()
            except:
                self._log.exception("Error while caching progress for ui")
            finally:
                self._update_progress.clear()
                self._update_progress.wait(0.45)

    def _calculate_progress(self):
        if __name__ == "__main__":
            progress = self._fakeProgress()
            progress = progress.values()
            progress = list(progress)
            progress_done = []

            total_rate = 0
        else:
            progress = []
            progress_done = []
            total_rate = 0

            current = ProgressHandler.current
            finished = ProgressHandler.finished

            for is_finished, tasks in (False, current), (True, finished):
                for task in reversed(tasks.values()):
                    task = task  # type: Progress

                    detail = UIDetail()
                    detail.filename = task.local_filename.decode() \
                        if isinstance(task.local_filename, bytes) else task.local_filename
                    detail.status = task.status
                    if task.status == task.STATUS_PURGE:
                        continue
                    detail.tooltip = str(task.error_msg) or "Finished"
    
                    filesize_units = ['B', 'KB', 'MB', 'GB']
                    detail.filesize = float(task.length)
                    while detail.filesize >= 1024 and len(filesize_units) > 1:
                        detail.filesize /= 1024
                        filesize_units = filesize_units[1:]
                    detail.filesize = "%s %s" % (("%4.2f" % detail.filesize)[0:5], filesize_units[0])
    
                    detail.percent = (100.0 * task.current / task.length) if task.length else 0
                    detail.last_update = task.updated
                    if is_finished:
                        detail.rate = 0
                    else:
                        rate = task.rate# / 1024 # KB/s
                        detail.rate = rate
                        total_rate += rate
    
                    detail_dict = detail.todict()
                    if is_finished:
                        progress_done.append(detail_dict)
                    else:
                        progress.append(detail_dict)

        details = dict(progress=progress,
                       finished=progress_done,
                       total_rate=total_rate,
                       status=ProgressHandler.status,
                       scanning=ProgressHandler.scanning)
        return details

    def _fakeProgress(self):
        fakeprogress = [
            {
                "copied": 8192,
                "status": "Copying",
                "filesize": 1326075505,
                "filename": "/myth/video/tvshows/Fear the Walking Dead/Season 1/Fear the Walking Dead S01E06 The Good Man.mkv",
                "last_update": 1444165920.6805089,
                "tracking": [
                    [
                        1444165920.6805089,
                        8192
                    ]
                ]
            },
            {
                "copied": 1739,
                "status": "Copying",
                "filesize": 1739,
                "filename": "/myth/video/tvshows/House/tvshow.nfo",
                "last_update": 1444165983.2005832,
                "tracking": [
                    [
                        1444165983.2005832,
                        1739
                    ]
                ]
            },
            {
                "copied": 1782,
                "status": "Copying",
                "filesize": 1782,
                "filename": "/myth/video/tvshows/Fear the Walking Dead/Season 1/Fear the Walking Dead S01E06 The Good Man.sfv",
                "last_update": 1444165984.0407848,
                "tracking": [
                    [
                        1444165984.0407848,
                        1782
                    ]
                ]
            }
        ]
        first = fakeprogress[0]
        first['copied'] = ((time.time() % 10) / 10) * int(first['filesize'])
        fakeprogress = OrderedDict({p['filename']: p for p in fakeprogress})
        return fakeprogress

    async def websocket_handler(self, request):

        ws = web.WebSocketResponse()
        await ws.prepare(request)

        request.app['websockets'].append(ws)
        self._update_progress.set()

        self._log.info('New ws connection, listeners: %d', len(request.app['websockets']))
        asyncio.ensure_future(self._ws_sender(ws))  # Kick off sender in the background

        try:
            async for msg in ws:
                if msg.type == aiohttp.WSMsgType.TEXT:
                    if msg.data == 'close':
                        await ws.close()
                    else:
                        # ws.send_str(msg.data + ' says the server')
                        # ws.send_str(json.dumps(self._progress()))
                        self._log.debug(msg.data)
                        data = json.loads(msg.data)

                        action = data.get('action')
                        if action == "refresh":
                            if self.feral:
                                self._log.critical("Manual refresh")
                                self.feral.sleep_event.set()
                        else:
                            name = data.get('name')
                            task = ProgressHandler.current.get(name) or ProgressHandler.finished.get(name)
                            if task:
                                if action == 'pause':
                                    await task.Pause()
                                elif action == 'resume':
                                    await task.Resume()
                                elif action == 'cleanup':
                                    await task.Cleanup()

                elif msg.type == aiohttp.WSMsgType.ERROR:
                    print('ws connection closed with exception %s' %
                          ws.exception())
                    break
        finally:
            await ws.close()
            request.app['websockets'].remove(ws)
            self._log.info('websocket connection closed, listeners: %d', len(request.app['websockets']))

        return ws

    async def _ws_sender(self, ws):
        """
        :param web.WebSocketResponse ws:
        :return: None
        """
        while not ws.closed:
            try:
                await ws.send_str(json.dumps(self._progress()))
                await asyncio.sleep(1)
            except:
                self._log.exception("Error in webapp")
                return

    # Settings
    async def settings_websocket(self, request):

        ws = web.WebSocketResponse()
        await ws.prepare(request)

        await self.send_settings(ws)
        asyncio.ensure_future(self.update_available(ws))

        try:
            async for msg in ws:
                if msg.type == aiohttp.WSMsgType.TEXT:
                    if msg.data == 'close':
                        await ws.close()
                    else:
                        data = json.loads(msg.data)
                        settings_data = data.get('settings')
                        if settings_data:
                            self._config_update(config.Config, settings_data)
                        if 'update' in data:
                            asyncio.ensure_future(self.do_update(ws))
                        if 'restart' in data:
                            asyncio.ensure_future(self.do_restart(ws))

                elif msg.type == aiohttp.WSMsgType.ERROR:
                    print('ws connection closed with exception %s' %
                          ws.exception())
                    break
        finally:
            await ws.close()
        return ws

    async def send_settings(self, ws):
        # send config and version
        config_details = config.Config.to_dict()
        config_details['version'] = Updater.__version__
        config_json = json.dumps({'settings': config_details})
        await ws.send_str(config_json)

    def _config_update(self, conf, dat):
        for key, val in dat.items():
            if (not key.startswith('_') and
                        key != '$$hashKey' and
                        key in conf):
                if isinstance(val, dict):
                    self._config_update(conf[key], val)
                elif isinstance(val, list):
                    for idx, lval in enumerate(val):
                        self._config_update(conf[key][idx], lval)
                else:
                    conf[key] = val

    async def settings_post(self, request):
        data = await request.json()
        self._config_update(config.Config, data)
        return web.Response()

    async def update_available(self, ws):
        avail = Updater.check_for_updates()
        await ws.send_str(json.dumps({'update_available': avail}))

    async def do_update(self, ws):
        Updater.update_package()
        
        await ws.send_str(json.dumps({'update_done': True}))
        await asyncio.sleep(1)
        if self.feral:
            self.feral.restart.set()
        asyncio.ensure_future(self.ShutdownAsyncServer())

    async def do_restart(self, ws):
        await ws.send_str(json.dumps({'update_done': True}))
        await asyncio.sleep(1)
        if self.feral:
            self.feral.restart()
        asyncio.ensure_future(self.ShutdownAsyncServer())


    # Service Runtime
    def _run(self, port):
        try:
            loop = asyncio.get_event_loop()
        except RuntimeError:
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)

        self._handler = self._app.make_handler()
        f = loop.create_server(self._handler, '0.0.0.0', port)
        srv = loop.run_until_complete(f)
        print('serving on', srv.sockets[0].getsockname())
        # self._app['cache_progress'] = self._app.loop.create_task(self._cache_progress())
        self._cache_progress_future = Future(self.thread_cache_progress)
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            pass
        finally:
            loop.run_until_complete(self._handler.finish_connections(1.0))
            srv.close()
            loop.run_until_complete(srv.wait_closed())
            loop.run_until_complete(self._app.shutdown())
            loop.run_until_complete(self._handler.finish_connections(60.0))
            loop.run_until_complete(self._app.cleanup())

            # self._app['cache_progress'].cancel()
            self._cache_progress_future.cancel()

        loop.close()

    async def StartAsyncServer(self, port=None):
        if port is None:
            port = config.Config.ui_server_port
        loop = asyncio.get_event_loop()
        self._handler = self._app.make_handler()
        startup = loop.create_server(self._handler, '0.0.0.0', port)
        self._srv = await startup
        self._cache_progress_future = Future(self.thread_cache_progress)
        self._log.info('serving on: ' + str(self._srv.sockets[0].getsockname()))

    def StartServer(self, port=None, debug=False):
        import threading
        if port is None:
            port = config.Config.ui_server_port
        if debug:
            self._run(port)
        else:
            thread = threading.Thread(target=lambda: self._run(port=port))
            thread.daemon = True
            thread.start()
            return thread

    async def ShutdownAsyncServer(self):
        if self._srv:
            self._srv.close()
            await self._srv.wait_closed()
            await self._app.shutdown()
            if self._handler:
                await self._handler.finish_connections(1.0)
            await self._app.cleanup()
            self._cache_progress_future.cancel()

            return True


if __name__ == "__main__":
    app = ProgressUI(None)
    app.StartServer(port=34203, debug=True)
