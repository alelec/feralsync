import importlib
from pathlib import Path
from setuptools_scm import get_version
from pkg_resources import get_distribution
try:
    from pip._internal.locations import USER_CACHE_DIR, src_prefix
    from pip._internal.commands.list import ListCommand
    from pip._internal.commands.list import get_installed_distributions
    from pip._internal.commands.install import InstallCommand
    from pip._internal.index import FormatControl
    from pip._internal.models.index import PyPI

except:
    from pip.locations import USER_CACHE_DIR, src_prefix
    from pip.commands.list import ListCommand
    from pip.commands.list import get_installed_distributions
    from pip.commands.install import InstallCommand
    from pip.index import FormatControl
    from pip.models.index import PyPI

UNKNOWN_VERSION = 'Unknown'
try:
    __version__ = get_version(str((Path(__file__).parent / '..').resolve())).split('+')[0]
except:
   # package is not installed
   __version__ = UNKNOWN_VERSION


class dotdict(dict):
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__
    __getattr__ = dict.__getitem__


class Updater(object):
    
    def __init__(self, package_name, extra_index):
        self.package_name = package_name
        self.extra_index = extra_index

        self._version = None

    @property
    def __version__(self):
        if self._version is None:
            try:
                self._version = get_distribution(self.package_name).version
            except:
                pkgdir = Path(__import__(self.package_name).__file__).parent
                self._version = get_version(str(pkgdir))
        return self._version

    def check_for_updates(self):
        pipListCommand = ListCommand()
    
        options = dotdict(verbose=0,
                          retries=5,
                          timeout=15,
                          no_input=True,
                          proxy='',
                          local=False,
                          user=False,
                          pre=False,
                          cert=None,
                          find_links=[],
                          client_cert=None,
                          no_index=False,
                          trusted_hosts=[],
                          cache_dir=USER_CACHE_DIR,
                          process_dependency_links=False,
                          disable_pip_version_check=True,
                          index_url=PyPI.simple_url,
                          extra_index_urls=[self.extra_index],
                          )
        packages = get_installed_distributions(
            local_only=False,
            user_only=False,
            editables_only=False,
        )
    
        package = [p for p in packages if p.key == self.package_name]
        outdated = pipListCommand.get_outdated(package, options)
    
        if self.package_name in [package.project_name for package in outdated]:
            return True
        return False
    
    def update_package(self):
        dry_run = False
        pipInstallCommand = InstallCommand()
        options = dotdict({'help': None, 'isolated_mode': False, 'require_venv': False, 'verbose': 0, 'version': None,
                           'quiet': 0, 'log': None, 'no_input': False, 'proxy': '', 'retries': 5, 'timeout': 15,
                           'default_vcs': '', 'skip_requirements_regex': '', 'exists_action': [], 'trusted_hosts': [],
                           'cert': None, 'client_cert': None, 'cache_dir': USER_CACHE_DIR,
                           'disable_pip_version_check': False, 'constraints': [], 'editables': [], 'requirements': [],
                           'build_dir': None, 'target_dir': None, 'download_dir': './tmp' if dry_run else None,
                           'src_dir': src_prefix, 'upgrade': True, 'upgrade_strategy': 'eager', 'force_reinstall': None,
                           'ignore_installed': None, 'ignore_requires_python': None, 'ignore_dependencies': False,
                           'install_options': None, 'global_options': None, 'use_user_site': None, 'as_egg': None,
                           'root_path': None, 'prefix_path': None, 'compile': True, 'use_wheel': True,
                           'format_control': FormatControl(no_binary=set(), only_binary=set()), 'pre': False,
                           'no_clean': False, 'require_hashes': False, 'index_url': PyPI.simple_url,
                           'extra_index_urls': [self.extra_index], 'no_index': False, 'find_links': [],
                           'process_dependency_links': False, 'allow_external': [], 'allow_all_external': False,
                           'allow_unverified': [], 'allow_all_insecure': False, 'build_options': None})
        pipInstallCommand.run(options, [self.package_name])
    

if __name__ == '__main__':
    import sys
    u = Updater(*sys.argv[1:])
    print(u.check_for_updates())
    u.update_package()

