import os
import ssl
import time
import json
import shutil
import logging
import hashlib
import asyncio
import aiohttp
import aiofiles
from urllib import parse
from numbers import Number
from urllib.parse import urlparse
from concurrent.futures import CancelledError
from async_timeout import timeout as async_timeout
from pathlib import Path
from abc import ABCMeta, abstractmethod
from http.client import responses as HTTP_CODES
from aiocontext import async_contextmanager

DOWNLOAD_CHUNK_SIZE_BYTES = 128 * 1024
MAX_OPEN_FILES = 512
TEMP_NAME='.part'

TIMEOUT = 3*60


def codestr(code):
    return HTTP_CODES.get(code, 'UNKNOWN')


class OperationFailed(Exception):
    _OPERATIONS = dict(
        HEAD = "get header",
        GET = "download",
        PUT = "upload",
        DELETE = "delete",
        MKCOL = "create directory",
        PROPFIND = "list directory",
        )

    def __init__(self, method, path, expected_code, actual_code):
        self.method = method
        self.path = path
        self.expected_code = expected_code
        self.actual_code = actual_code
        operation_name = self._OPERATIONS[method]
        self.reason = 'Failed to {operation_name} "{path}"'.format(**locals())
        expected_codes = (expected_code,) if isinstance(expected_code, Number) else expected_code
        expected_codes_str = ", ".join('{0} {1}'.format(code, codestr(code)) for code in expected_codes)
        actual_code_str = codestr(actual_code)
        msg = '''\
{self.reason}.
  Operation     :  {method} {path}
  Expected code :  {expected_codes_str}
  Actual code   :  {actual_code} {actual_code_str}'''.format(**locals())
        super(OperationFailed, self).__init__(msg)


class Progress(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self.enabled = asyncio.Event()
        self.enabled.set()

    @abstractmethod
    async def progress_update(self, add_size):
        """
        :param int add_size: chunk size just downloaded
        :return: None
        """
        pass

    @abstractmethod
    async def notify_done(self, error):
        """
        Called when the download task is finished
        :param (str | None) error: Error string if there was an issue or None
        :return: None
        """
        pass

    @abstractmethod
    async def notify_verifying(self):
        """
        Called when the download transfer is complete and about to be verified
        :return: None
        """
        pass


class Client(object):

    def __init__(self, url=None, host=None, port=0, auth=None, username=None, password=None,
                 protocol='http', verify_ssl=True, path=None, cert=None, max_connections=30):
        self.log = logging.getLogger("%s.%s" % (self.__class__.__module__, self.__class__.__name__))

        self.basepath = ''
        if url:
            self.baseurl = url
            self.basepath = urlparse(url).path
        else:
            if not port:
                port = 443 if protocol == 'https' else 80
            self.baseurl = '{0}://{1}:{2}'.format(protocol, host, port)
            if path:
                self.baseurl = '{0}/{1}'.format(self.baseurl, path)
                self.basepath = path
        self.cwd = '/'

        self._cert = cert
        self._verify_ssl = verify_ssl
        self._max_connectors = max_connections

        self.auth = auth
        if not auth and username and password:
            self.auth = aiohttp.BasicAuth(username, password=password)

        self._closed = False

        self.limit_files = asyncio.Semaphore(MAX_OPEN_FILES)
        self.limit_gets = asyncio.Semaphore(max_connections)

    def __del__(self):
        self.close()

    @property
    def session(self):
        session = aiohttp.ClientSession(connector=self.connector, auth=self.auth)
        if self._cert:
            self.session.cert = self._cert
        return session

    @property
    def connector(self):
        sslcontext = None
        if self._cert:
            sslcontext = ssl.create_default_context(cafile=self._cert)
        connector = aiohttp.TCPConnector(ssl_context=sslcontext,
                                         verify_ssl=self._verify_ssl,
                                         limit=self._max_connectors*4) # we're relying on the local limit mostly
        return connector

    def close(self):
        self._closed = True

    def _get_url(self, path):
        if path.startswith('/'):
            return self.baseurl + path
        return parse.quote("".join((self.baseurl, self.cwd, path)))

    def cd(self, path):
        path = path.strip()
        if not path:
            return
        stripped_path = '/'.join(part for part in path.split('/') if part) + '/'
        if stripped_path == '/':
            self.cwd = stripped_path
        elif path.startswith('/'):
            self.cwd = '/' + stripped_path
        else:
            self.cwd += stripped_path

    async def rmdir(self, path):
        path = str(path).rstrip('/') + '/'
        await self.delete(path)

    async def delete(self, path):
        """
        :param (File | str) path: path from server root or File object to delete
        :return: None
        """
        async with self._delete(path) as request:
            async with request as resp:
                try:
                    resp.raise_for_status()
                except aiohttp.http.HttpProcessingError as ex:
                    if ex.code == 404:
                        pass
                    else:
                        raise

    async def _check_existing_download(self, local_path, remote_file, start, end):
        overlap = 16
        mode = 'r+b' if os.path.exists(local_path) else 'w+b'
        try:
            async with self.limit_files:
                length1 = os.path.getsize(local_path)
                async with aiofiles.open(local_path, mode) as fileobj:

                    length = await fileobj.seek(0, os.SEEK_END)
                    valid = 0
                    if length and length <= (end-start+1):
                        await fileobj.seek(0)
                        head = await fileobj.read(overlap)
                        length2 = os.path.getsize(local_path)
                        headers = {"Range": "bytes=%d-%d"%(start,start+overlap-1)}

                        async with self.limit_gets:
                            async with self._get(remote_file, headers=headers) as request:
                                async with request as rsp:
                                    read = await rsp.content.read()
                                    if read == head:
                                        valid = length

                        if valid:
                            valid = False
                            pos = await fileobj.seek(length-overlap) + start
                            tail = await fileobj.read()
                            headers = {"Range": "bytes=%d-%d"%(pos,pos+overlap-1)}
                            async with self.limit_gets:
                                async with self._get(remote_file, headers=headers) as request:
                                    async with request as rsp:
                                        read = await rsp.content.read()
                                        if read == tail:
                                            valid = length

                    if not valid:
                        await fileobj.seek(0)
                        await fileobj.truncate(0)
        except OSError as ex:
            if aiohttp.ClientOSError:
                raise
            logging.error("couldn't parse existing file %s, resetting", local_path)
            valid = 0

        return valid

    async def _download_stream(self, local_path, part_path, remote_file, start, end, progress):
        ex = Exception('broken')
        finished = False
        pos = existing = 0
        exceptions = []
        retry = 10
        while not finished and retry:
            retry -= 1
            try:
                if progress:
                    await progress.enabled.wait()

                exists = os.path.exists(str(part_path))
                if not exists:
                    with open(str(part_path), 'wb'):
                        "Simply open the file to create it"

                if isinstance(local_path, str):

                    if exists:
                        pos = await self._check_existing_download(str(part_path), remote_file, start, end)
                    if progress:
                        await progress.progress_update(pos)
                    req_start = start + pos
                    if req_start >= end:
                        finished = True

                else:
                    req_start = start

                if not isinstance(end, int) or req_start < end:

                    header = {"Range": "bytes=%s-%s" % (req_start, end)}
                    async with self.limit_gets:
                        with Timeout(TIMEOUT) as timeout:
                            async with self._get(remote_file, chunked=True, headers=header, timeout=None) as request:
                                async with request as response:

                                    timeout.reset()
                                    while not progress or progress.enabled.is_set():

                                        chunk = await response.content.read(DOWNLOAD_CHUNK_SIZE_BYTES)
                                        if not chunk:
                                            finished = True
                                            break

                                        timeout.reset()
                                        if isinstance(local_path, (str, Path)):
                                            async with self.limit_files:
                                                async with aiofiles.open(str(part_path), 'r+b') as fileobj:
                                                    await fileobj.seek(pos)
                                                    length = len(chunk)
                                                    await fileobj.write(chunk)
                                                    pos += length
                                        else:
                                            length = len(chunk)
                                            local_path.write(chunk)
                                            pos += length

                                        if progress:
                                            await progress.progress_update(length)

            except (TimeoutException, asyncio.TimeoutError, ConnectionResetError, CancelledError,
                    aiohttp.ServerDisconnectedError, aiohttp.ClientError,
                    aiohttp.ClientResponseError, aiohttp.ClientOSError) as ex:
                exceptions.append(ex)

            except Exception as ex:
                exceptions.append(ex)
                logging.exception("Exception in download")

            finally:
                if not finished:
                    await asyncio.sleep(1) # Rate limiting
                    if not retry and exceptions:
                        logging.error("Not retrying anymore")
                        raise exceptions[-1]  # somewhere to breakpoint

        if finished and exceptions:
            self.log.warning('download worked after retries with exceptions %s', str(exceptions))
        elif not finished:
            self.log.error('download %s failed with exceptions %s', part_path, str(exceptions))

        return part_path, pos - existing

    @staticmethod
    def join_parts(local_path, partfiles):
        """
        This is run in an executor so don't worry about blocking main loop
        :param local_path:
        :param partfiles:
        :return:
        """
        # Join all parts together
        with open(local_path, 'wb') as local_file:
            for part in partfiles:
                with open(part, 'rb') as partfile:
                    shutil.copyfileobj(partfile, local_file)

    async def download(self, remote_file, remote_size, local_path, progress=None):
        """
        :param str local_path: path to write local file
        :param remote_file: remote file path/name
        :param remote_size: remote file size
        :param (Progress or None) progress: optional class of callbacks which gets notified of progress
        :return:
        """
        error = 'Unknown'
        responses = []
        loop = asyncio.get_event_loop()
        downloads = asyncio.Queue()

        try:
            # expected_length = remote_size

            chunks = []
            chunksize = (10 * 1000 * 1000)

            if isinstance(local_path, str):
                # Download to local filename in multiple part files

                dirname = os.path.dirname(local_path)
                if not dirname:
                    dirname = '.'
                if not os.path.exists(dirname):
                    os.makedirs(dirname)

                pad = len(str(remote_size))
                partname_tmpl = os.path.join(os.path.dirname(local_path), (".%s.{0:0%dd}%s" % (os.path.basename(local_path), pad, TEMP_NAME)))
                for start in range(0, remote_size, chunksize):
                    end = start + chunksize - 1
                    if end >= remote_size:
                        end = remote_size - 1

                    partname = str(partname_tmpl).format(start)
                    await downloads.put(self._download_stream(local_path, partname, remote_file, start, end, progress))

            else:  # Assume local_path is a file-like object to stream into
                await downloads.put(self._download_stream(local_path, "stream", remote_file, 0, remote_size, progress))

            async def _worker():
                while not downloads.empty():
                    fn = await downloads.get()
                    part, dl_size = await fn
                    responses.append(part)
            await asyncio.gather(*[_worker() for _ in range(self._max_connectors)])

            partfiles = sorted(responses)

            if remote_size == 0:
                error = "Null file reported"
            else:
                logging.info('Checking: %s', local_path)

                await progress.notify_verifying()

                if isinstance(local_path, str):  # Check and re-assemble part files
                    local_temp = local_path + TEMP_NAME
                    if sum([os.path.getsize(f) for f in partfiles]) == remote_size:
                        error = None
                        if len(partfiles) > 1:
                            async with self.limit_files:
                                await loop.run_in_executor(None, self.join_parts, local_temp, partfiles)
                        else:
                            os.rename(partfiles[0], local_temp)

                        if not await self.verify(remote_file, remote_size, local_temp):
                            error = "Invalid Checksum:  %s" % str(remote_file)
                            os.rename(local_temp, local_path + ".invalid")
                            await self.delete(remote_file+'.md5')
                        else:
                            for part in partfiles:
                                if os.path.exists(part):
                                    os.unlink(part)
                    else:
                        error = 'downloaded data does not match expected length'

                    if not error:
                        os.rename(local_temp, local_path)

        except Exception as ex:
            tb = __import__('traceback').format_exc()
            error = str(ex)
            if not error:
                error = type(ex)

        return error

    async def verify(self, remote_file, remote_size, local_file):
        logging.info('verifying: %s', local_file)
        if not os.path.exists(local_file):
            return False
        if os.path.getsize(local_file) != remote_size:
            return False

        loop = asyncio.get_event_loop()
        remote_hash, local_md5 = await asyncio.gather(self.checksum(remote_file),
                                         loop.run_in_executor(None, self.md5, local_file))

        if remote_hash:
            remote_md5 = remote_hash[0:remote_hash.find(b' ')]

            remote_md5 = remote_md5.decode() if isinstance(remote_md5, bytes) else remote_md5
            local_md5 = local_md5.decode() if isinstance(local_md5, bytes) else local_md5

            if local_md5 == remote_md5:
                return True

    @staticmethod
    def md5(fname):
        """
        This is run in a executor so we shouldn't need to worry about blocking the event loop
        :param fname:
        :return:
        """
        with open(fname, "rb") as f:
            return Client._md5(f)


    @staticmethod
    def _md5(fileobj):
        chunksize = 5*1024*1024
        start = fileobj.tell()
        hash_md5 = hashlib.md5()
        for chunk in iter(lambda: fileobj.read(chunksize), b""):
            hash_md5.update(chunk)
        fileobj.seek(start)
        return hash_md5.hexdigest()


    async def checksum(self, remote_path):
        """
        :param str remote_path: path relative to the server to list
        :return:
        """
        md5 = None
        async with self.limit_gets:
            async with self._get(remote_path+'.md5', timeout=TIMEOUT*2) as request:
                async with request as response:
                    try:
                        response.raise_for_status()
                        md5 = (await response.read()).strip()
                    except Exception as ex:
                        if response and response.status == 404:
                            self.log.error('Tried to get missing checksum: %s', remote_path)
                        else:
                            raise
        return md5

    async def ls(self, remote_path=''):
        """
        :param str remote_path: path relative to the server to list
        :return: {} of remote tree
        """
        started = time.time()
        try:
            async with self._get(remote_path.rstrip('/'), urlargs='json') as request:
                async with request as response:
                    # Redirect
                    if response.status == 301:
                        url = urlparse(response.headers['location'])
                        return self.ls(url.path)
                    try:
                        response.raise_for_status()
                        entries = await response.json()
                    except json.decoder.JSONDecodeError:
                        entries = None
                        if '/login?' in response.url:
                            self.log.error("Invalid Authentication")
                        else:
                            raise
                    except Exception as ex:
                        entries = None
                        if response and response.status == 404:
                            self.log.error('Tried to list missing folder: %s', remote_path)
                        else:
                            raise

        except asyncio.TimeoutError:
            taken = time.time() - started
            self.log.error('ls asyncio timeout after %d seconds', int(taken))
            entries = {}

        except TimeoutError:
            taken = time.time() - started
            self.log.error('ls timeout after %d seconds', int(taken))
            entries = {}

        return entries

    async def exists(self, remote_path):
        url = self._get_url(remote_path)
        async with self.session as session:
            async with await session.head(url) as response:
                ret =  True if response.status != 404 else False
        return ret

    @async_contextmanager
    async def _get(self, remote_path, urlargs=None, **kwargs):
        """
        :param remote_path: file to get
        :param kwargs: options kwargs to send to request
        :return: coroutine to send get request
        """
        url = self._get_url(remote_path)
        if urlargs:
            url += "?%s" % urlargs
        kwargs['timeout'] = kwargs.get('timeout', TIMEOUT)
        async with self.session as session:
            yield session.get(url, **kwargs)

    @async_contextmanager
    async def _delete(self, remote_path, **kwargs):
        """
        :param remote_path: file to delete
        :param kwargs: options kwargs to send to request
        :return: coroutine to send delete request
        """
        url = self._get_url(remote_path)
        async with self.session as session:
            yield session.delete(url, **kwargs)


class TimeoutException(asyncio.TimeoutError):
    def __init__(self, resetted):
        self.resetted = resetted
        super(TimeoutException, self).__init__()

class Timeout(async_timeout):
    """
    Extended version of async_timeout with a reset function
    """
    def __init__(self, timeout, *, loop=None):
        super(Timeout, self).__init__(timeout, loop=loop)
        self._resetted = 0

    def reset(self, timeout=None):
        if None not in (self._cancel_handler, self._timeout):
            self.cancel()
            self._cancel_handler = self._loop.call_later(
                timeout or self._timeout, self._cancel_task)
            self._resetted += 1

    def cancel(self):
        if None not in (self._cancel_handler, self._timeout):
            self._cancel_handler.cancel()
            self._cancel_handler = None

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is asyncio.CancelledError and self._cancelled:
            self._cancel_handler = None
            self._task = None
            raise TimeoutException(self._resetted)
        if self._timeout is not None:
            self._cancel_handler.cancel()
            self._cancel_handler = None
        self._task = None

