
var opts = {
  lines: 7 // The number of lines to draw
, length: 4 // The length of each line
, width: 3 // The line thickness
, radius: 4 // The radius of the inner circle
, scale: 0.8 // Scales overall size of the spinner
, corners: 1 // Corner roundness (0..1)
, color: '#000' // #rgb or #rrggbb or array of colors
, opacity: 0.25 // Opacity of the lines
, rotate: 0 // The rotation offset
, direction: 1 // 1: clockwise, -1: counterclockwise
, speed: 1 // Rounds per second
, trail: 60 // Afterglow percentage
, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
, zIndex: 2e9 // The z-index (defaults to 2000000000)
, className: 'spinner' // The CSS class to assign to the spinner
, top: '100%' // Top position relative to parent
, left: '70%' // Left position relative to parent
, shadow: false // Whether to render a shadow
, hwaccel: false // Whether to use hardware acceleration
, position: 'relative' // Element positioning
}

var spinner = new Spinner(opts);
spinner.stop();

var settings_spinner = new Spinner(opts);
settings_spinner.stop();


angular

    .module('FeralSync', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'ui.bootstrap.modal'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/a_home.html',
                controller: 'FeralSyncController'
            })
            .otherwise({ redirectTo: '/' });
    }])

    .controller('FeralSyncController', function ($scope, $modal, $http) {
        $scope.percent = 50.0;
        $scope.progress = [];
        $scope.finished = [];
        $scope.total_download = 0;

        var websocket = new WebSocket('ws://'+window.location.host+'/ws');
        websocket.onopen = function () {
        };
        websocket.onclose = function () {
        };
        websocket.onmessage = function (evt) {
            var response = JSON.parse(evt.data);
            $scope.$apply(function () {
                Object.keys(response).forEach(function(key, index) {

                    function merge_lists(key) {
                        var new_names = [];
                        response[key].forEach(function(task){
                            var existing;
                            new_names.push(task.filename);
                            $scope[key].forEach(function (exist) {
                               if (exist.filename == task.filename) {
                                   existing = exist;
                               }
                            });

                            if (existing === undefined) {
                                $scope[key].push(task);
                            } else {
                                var equal = true;
                                ['filesize', 'percent', 'rate', 'status', 'tooltip']
                                    .forEach(function (k) {
                                        if (task[k] != existing[k]){
                                            equal = false;
                                        }
                                    });
                                if (!equal) {
                                    for (var k in task) {
                                        existing[k] = task[k]
                                    }
                                }
                            }
                        });

                        for (var idx = 0; idx < $scope[key].length; idx++) {
                            var task = $scope[key][idx];
                            if (task !== undefined) {
                                if (new_names.indexOf(task.filename) == -1)
                                {
                                    $scope[key].splice(idx, 1);
                                }
                            }
                        }
                    }

                    if ((key == 'progress') || (key == 'finished')) {
                        merge_lists(key);
                    } else {
                        $scope[key] = response[key];
                    }
                });
            });

            if ($scope.scanning) {
                spinner.spin(document.getElementById('scanning'));
                $scope.scanning_text = ''; // This updates at a different rate 'scanning...';
            } else {
                spinner.stop();
                $scope.scanning_text = '';
            }
        };

        $scope.pause = function(item) {
            websocket.send(JSON.stringify({'action':'pause','name':item}));
        };
        $scope.resume = function(item) {
            websocket.send(JSON.stringify({'action':'resume','name':item}));
        };
        $scope.cleanup = function(item) {
            websocket.send(JSON.stringify({'action':'cleanup','name':item}));
        };
        $scope.refresh = function() {
            websocket.send(JSON.stringify({'action':'refresh'}));
        };

        $scope.websocket = websocket;

        $scope.settings_dlg = function() {



            // $http.get('/settings')
            //     .then(function successCallback(rsp){
            //
            //         $scope.settings = rsp.data;

                    var modalInstance = $modal.open({
                        backdrop: true,
                        backdropClick: true,
                        dialogFade: false,
                        keyboard: true,
                        templateUrl : 'settings.html',
                        controller : 'Settings'//,
                        // resolve: { item:
                        //     function() {
                        //         return angular.copy({settings: $scope.settings});
                        //     }
                        // }
                    });

                    modalInstance.result.then(function(){
                        //on ok button press
                        console.log("Modal Closed OK");
                    },function(){
                        //on cancel button press
                        console.log("Modal Closed Cancel");
                    });
                // });
        };

        $scope.transmission_dlg = function() {

            var modalInstance = $modal.open({
                backdrop: true,
                backdropClick: true,
                dialogFade: false,
                keyboard: true,
                templateUrl: 'add_torrent.html',
                controller: 'AddTorrent'
            });

            modalInstance.result.then(function () {
                //on ok button press
                console.log("Modal Closed OK");
            }, function () {
                //on cancel button press
                console.log("Modal Closed Cancel");
            });
        };

        $scope.dropzone_visible = 0;

        $scope.$on('dropzone_hide', function(event){
            $scope.dropzone_visible --;
            if ($scope.dropzone_visible < 0) {
                $scope.dropzone_visible = 0
            }
        });

        window.addEventListener("dragenter", function(e) {
            $scope.dropzone_visible ++;
        });

        window.addEventListener("dragleave", function(e) {
            $scope.dropzone_visible --;
            if ($scope.dropzone_visible < 0) {
                $scope.dropzone_visible = 0
            }
        });


    })

    .controller("AddTorrent", function($scope, $modalInstance, $http) {

         $scope.transmission = {url: "",
                                file: "",
                                type: "movies"};

         $scope.set_file = function (element) {
            file = element.files[0];

            if (file) {
                var reader = new FileReader();

                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    $scope.transmission.file = btoa(binaryString);
                };

                reader.readAsBinaryString(file);
            }
         };

         $scope.ok = function () {
            $http.post('/add_torrent', JSON.stringify($scope.transmission));
            $modalInstance.close();
         };

         $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
         };
    })

    .controller("Settings", function($scope, $modalInstance) {

         // $scope.item = item;
         // $scope.settings = item.settings;

        var websocket = new WebSocket('ws://'+window.location.host+'/settings');
        websocket.onopen = function () {
        };
        websocket.onclose = function () {
        };
        websocket.onmessage = function (evt) {
            var response = JSON.parse(evt.data);
            $scope.$apply(function () {
                Object.keys(response).forEach(function(key, index) {
                    if (key == 'settings') {
                        $scope.settings = response[key]
                    } else if (key == 'update_available') {
                        $scope.update_available = response[key]
                    }
                });
            });

            if ($scope.update_available === undefined) {
                settings_spinner.spin(document.getElementById('settings_spin'));
            } else {
                settings_spinner.stop();
            }
        };

          $scope.ok = function () {
              // $http.post('/settings', JSON.stringify($scope.settings));
              websocket.send(JSON.stringify({'settings':$scope.settings}));
              $modalInstance.close();
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.restart = function () {
              websocket.send(JSON.stringify({'restart':true}));
              $modalInstance.close();
              setTimeout(function(){location.reload();}, 1500)
          };
          $scope.update = function () {
              websocket.send(JSON.stringify({'update':true}));
          };
    })

    // .controller('Settings', function($scope, $html) {
    // })

    .directive('filename', function () {
        return ({
            link: function(scope, elem, attr, ctrl) {

                var element = angular.element(elem);
                var fn_head = angular.element(element[0].getElementsByClassName('filename_head'));
                var fn_fade = angular.element(element[0].getElementsByClassName('filename_fade'));
                var fn_tail = angular.element(element[0].getElementsByClassName('filename_tail_inner'));
                var fn_long = angular.element(element[0].getElementsByClassName('filename_long'));
                var width = element.width();

                scope.getFilenameWidth = function () {
                  return fn_long.width();
                };

                scope.$watch(scope.getFilenameWidth, function (newValue, oldValue) {
                    var long_width = newValue;
                     if(long_width <= width) {
                         fn_head.addClass('fullwidth');
                         fn_head.addClass('text_left');
                         fn_fade.addClass('collapse');
                         fn_tail.addClass('collapse');
                     }

                }, true);
            }
        });
    })

    .directive('limitFilename', function ($timeout) {
        return function(scope, element, attrs) {
            //var maxLen = attrs.maxLen;
            var el = angular.element(element);
            $timeout(function () {
                var width = el.width();
                var parent = el.parent();
                var par_width = parent.width();
                if(width > par_width) {
                    el.addClass('text_right');
                    parent.addClass('overflow_fade');
                    parent.height(el.height());
                }

            });
        };
    })

    .filter('limit_width', function () {
        return function (value, chars) {
            if (!value) return '';

            chars = parseInt(chars, 20);
            if (!chars) return value;
            if (value.length <= chars) return value;

            var pre_cut = (value.length - chars - 5) / 2;
            var post_cut = (value.length - chars - 5) - pre_cut;
            var cutpos = (value.length / 3);
            value = value.substr(0, cutpos-pre_cut) + " ... " + value.substr(post_cut);

            return value;
        };
    })

    .directive('tooltipLoader', function () {
        return function(scope, element, attrs)
            {
                //element
                //    .attr('title',scope.$eval(attrs.tooltip))
                //    .tooltip({placement: "right"});
                element.tooltip({
                    trigger:"hover",
                    placement: "top",
                    delay: {show: 1000, hide: 0}
                  });
            }
    })

    .directive('filedropzone', function ($log, $http) {
        return {
            restrict: 'A',
            scope: {
                dropfiles: '=' //One way to return your drop file data to your controller
            },
            link: function (scope, element, attrs) {
                scope.dropcounter = {};

                var processDragEnter = function (event) {
                    if (scope.dropcounter[attrs.filedropzone] === undefined) {
                        scope.dropcounter[attrs.filedropzone] = 0;
                    }
                    scope.dropcounter[attrs.filedropzone]++;
                    element.addClass("dropover");
                    return true;
                };
                var processDragLeave = function (event) {
                    scope.dropcounter[attrs.filedropzone]--;
                    if (scope.dropcounter[attrs.filedropzone] <= 0) {
                        element.removeClass("dropover");
                        scope.dropcounter[attrs.filedropzone] = 0;
                    }
                    return true;
                };

                element.bind('dragleave', processDragLeave);
                element.bind('dragenter', processDragEnter);

                // dragover needs to be cancelled to allow drop to work
                element.bind('dragover', function(event){
                    event.preventDefault();
                });

                return element.bind('drop', function (event) {
                    try {
                        // Clear popup
                        processDragLeave(event);
                        scope.$emit('dropzone_hide');

                        // Handle files
                        var files = [];
                        scope.dropfiles = [];

                        if (event != null) {
                            event.preventDefault();
                        }

                        var fileCount = 0;
                        angular.forEach(event.originalEvent.dataTransfer.files, function (item) {
                            if (fileCount < 10) {  //Can add a variety of file validations
                                files.push(item);
                            }
                            fileCount++;
                        });

                        files.forEach(function (item) {
                            var reader = new FileReader();

                            reader.readAsDataURL(item);

                            reader.onload = function(readerEvt) {
                                var binaryString = readerEvt.target.result;
                                var transmission = {url: "",
                                                 file: binaryString,
                                                 type: attrs.filedropzone};
                                $http.post('/add_torrent', JSON.stringify(transmission));
                            };

                        });

                    }
                    catch (err) {
                        $log.info("File Drop Zone: " + err)
                    }

                });

            }
        };
    })

    .filter('display_rate', function () {
        return function (rate) {
            var rate_units;
            var rate_kbs = rate/1024;
            if (rate_kbs > 1024) {
                rate_kbs = rate_kbs / 1024;
                rate_units = ' MB/s'
            } else {
                rate_units = ' KB/s'
            }
            var precision = 2;
            if (rate_kbs >= 100) {
                precision = 0;
            } else if (rate_kbs >= 10) {
                precision = 1;
            }
            var power = Math.pow(10, precision || 0);
            return String(Math.round(rate_kbs * power) / power) + rate_units;
        };
    });

