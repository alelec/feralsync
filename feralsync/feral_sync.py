###############################################################
# -*- coding: utf-8 -*-
#!/usr/bin/env python3
__author__ = "Andrew Leech"
__copyright__ = "Copyright 2017, alelec"
__license__ = "GPL"
__maintainer__ = "Andrew Leech"
__email__ = "andrew@alelec.net"
__status__ = "Development"

import os

import sys
import time
import signal
import asyncio
import logging
import aiohttp
import inspect
import importlib
from aiohttp import web
from collections import namedtuple
from concurrent.futures import CancelledError
try:
    from asyncio import JoinableQueue as Queue
except ImportError:
    from asyncio import Queue

import feralsync as fs

loaded = time.time()


def reload():
    for _, mod in inspect.getmembers(fs, inspect.ismodule):
        importlib.reload(mod)
    importlib.reload(fs)

# try:
#     import uvloop
#     asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
# except ImportError:
#     # Don't care if uvloop isn't installed, we just wont use it.
#     uvloop = None

try:
    import raven
    sentry = raven.Client(fs.Config.raven_dsn)
except ImportError:
    sentry = raven = None


class dotdict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

IDLE_TIME = 1200  # 20 minutes between scans by default


file_detail = namedtuple('file_detail', ('remote_fname', 'fsize', 'local_fname'))


class CheckableAsyncQueue(asyncio.Queue):
    def __contains__(self, item):
        """Check the Queue for the existence of an item."""
        return item in self._queue


class FeralSync:
    def __init__(self, daemon = True):
        self.daemon = daemon
        self.tasks = CheckableAsyncQueue()
        self.skipped = []

        self.ui = fs.webapp.ProgressUI(self)

        self.max_connections = fs.Config.concurrent_connections
        self.concurrent_transfers = fs.Config.concurrent_transfers

        self.sleep_event = asyncio.Event()

        self.run_future = None  # asyncio.Task
        self.restarting = False

    def restart(self):
        self.restarting = True
        self.run_future.cancel()

    @staticmethod
    def open_browser():
        time.sleep(2)
        import webbrowser
        webbrowser.open('http://localhost:{}'.format(fs.Config.ui_server_port))

    async def run(self):
        logging.info("Software Startup")

        await self.ui.StartAsyncServer()
        # self.ui.StartServer(debug=False)

        asyncio.ensure_future(fs.ProgressHandler.rate_calc())
        asyncio.ensure_future(fs.ProgressHandler.task_cleanup())

        if fs.Config.browser_on_launch:
            fs.future_thread.Future(self.open_browser)

        while True:
            try:
                for _ in range(self.concurrent_transfers):
                    asyncio.ensure_future(self.worker())

                await asyncio.gather(*[self.sync_target(target) for target in fs.Config.sync_targets])

            except KeyboardInterrupt:
                fs.ProgressHandler.close()
                await self.ui.ShutdownAsyncServer()
                break

            except CancelledError:
                fs.ProgressHandler.close()
                await self.ui.ShutdownAsyncServer()
                break

            except:
                logging.exception("Main loop failed")
                time.sleep(3)

    async def sync_target(self, target):
        """
        :param config.Target target:
        :return:
        """
        sync = target
        log = logging.getLogger('sync_target')

        while True:
            try:
                await self._sync_folder(sync.remote_path, sync.local_path)
                self.sleep_event.clear()

            except:
                log.exception("Unexpected")

            finally:
                if not self.daemon:
                    break
                try:
                    await asyncio.wait_for(self.sleep_event.wait(), IDLE_TIME)
                except asyncio.TimeoutError:
                    pass

    def client(self):
        url = fs.Config.server.url
        username = fs.Config.server.username
        password = fs.Config.server.password
        if not url.rstrip('/').endswith(username):
            url = '/'.join((url.rstrip('/'), username))

        if username == '<user>':
            client = None
            fs.ProgressHandler.status = "Please configure setting (upper right hand corner)"
        else:
            client = fs.Client(url=url, username=username, password=password,
                               max_connections=self.max_connections)
        return client

    async def _sync_folder(self, remote_path, local_path):
        client = self.client()
        if client:
            entries = {}
            message = None
            try:
                fs.ProgressHandler.scanning = True
                entries = await client.ls(remote_path)

            except web.HTTPNotFound:
                message = 'Tried to list missing folder: %s' % remote_path
                logging.error(message)

            except aiohttp.ClientOSError:
                message = 'Remote connection error during ls.'
                logging.error(message)

            except ConnectionResetError:
                message = 'Remote connection reset during ls.'
                logging.error(message)

            except aiohttp.http.HttpProcessingError as ex:
                if '401' in ex.message or 'Unauthorized' in ex.message:
                    message = "Authentication Error"
                else:
                    message = ex.message
                logging.exception(message)

            except Exception as ex:
                message = 'Exception in ls: %s' % ex
                logging.exception(message)

            finally:
                fs.ProgressHandler.scanning = False

            if message:
                fs.ProgressHandler.status = message

            if entries:
                await self._download_folder(entries, remote_path, local_path)
            return entries is not None

    @staticmethod
    async def process(client, task, progress):
        try:
            if os.path.exists(task.local_fname) and \
                            os.path.getsize(task.local_fname) == task.fsize and \
                    await client.verify(task.remote_fname, task.fsize, task.local_fname):

                # already downloaded
                error = None
            else:
                error = await client.download(task.remote_fname, task.fsize,
                                              task.local_fname, progress=progress)
            await progress.notify_done(error)

            if not error:
                await client.delete(task.remote_fname)

        except:
            logging.exception('Error in download')

    async def worker(self):
        while True:
            client = self.client()
            if not client:
                await asyncio.sleep(5)
            else:
                fs.ProgressHandler.status = "Backlog: %d" % self.tasks.qsize()
                task = await self.tasks.get()  # type: file_detail
                if (task.local_fname not in fs.ProgressHandler.current and
                            task.local_fname not in fs.ProgressHandler.finished):
                    progress = fs.ProgressHandler.register(remote_file=task.remote_fname,
                                                           remote_size=task.fsize,
                                                           local_filename=task.local_fname)

                    asyncio.ensure_future(self.process(client, task, progress))
                    await progress.done.wait()

    async def _download_folder(self, entries, remote_path, local):
        folder_tasks = []
        for entry, detail in sorted(entries.items()):

            isfolder = isinstance(detail, dict)
            if isfolder:
                folder = entry
                if folder == '.':
                    remote_subpath = remote_path
                    local_subpath = local
                else:
                    remote_subpath = '/'.join((remote_path.rstrip('/'), folder))
                    local_subpath = os.path.join(local, folder)

                subfolder_tasks = await self._download_folder(detail, remote_subpath, local_subpath)
                folder_tasks.extend(subfolder_tasks)

            elif isinstance(detail, int):
                fname, fsize = entry, detail
                remote_fname = '/'.join((remote_path.rstrip('/'), fname))
                local_fname = os.path.join(local, fname)
                if local_fname.endswith('.md5'):
                    continue

                if local_fname not in self.tasks and local_fname not in self.skipped:
                    # Register download
                    task = file_detail(remote_fname, fsize, local_fname)

                    if task not in self.tasks:
                        await self.tasks.put(task)

            else:
                logging.error("%s:%s" % (entry, detail))

        return folder_tasks


def exit_handler(_signo=None, _stack_frame=None):
    sys.stdout.write('\n')
    sys.exit(0)


def run_feral_sync():
    try:
        loop = asyncio.get_event_loop()
    except RuntimeError:
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

    while True:
        feral = fs.feral_sync.FeralSync()
        try:
            feral.run_future = asyncio.ensure_future(feral.run())
            loop.run_until_complete(feral.run_future)
        except RuntimeError:
            pass  # This is expected on forceful shutdown of runtime loop
        if not feral.restarting:
            break
        reload()


def main():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    hdlr = logging.StreamHandler()
    hdlr.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)

    signal.signal(signal.SIGTERM, exit_handler)

    run_feral_sync()

    exit_handler()

if __name__ == "__main__":
    main()
