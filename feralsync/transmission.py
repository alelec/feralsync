import base64
import transmissionrpc
from pathlib import Path
from urllib.parse import urlparse, urlunsplit

from . import config


def rpc_url():
    user = config.Config.server.username
    fileserver_server = urlparse(config.Config.server.url)
    path = "/corona/{user}/transmission/rpc".format(user=user)
    transmission_url = urlunsplit(fileserver_server[0:2] + (path, '', ''))
    return transmission_url


def add_torrent(url=None, torrent=None, folder=None):
    user = config.Config.server.username
    password = config.Config.server.password

    client = transmissionrpc.Client(address=rpc_url(), user=user, password=password)

    default_dir = Path(client.get_session().download_dir)

    torrent = url if url else torrent
    head = torrent[0:64]
    if head.startswith('data:'):
        torrent = torrent[head.index('base64,')+7:]
    folder = "finished" if folder not in ("tvshows", "movies") else folder
    dl_path = default_dir.parent / folder
    torr = client.add_torrent(torrent=torrent, download_dir=str(dl_path))

    return torr
